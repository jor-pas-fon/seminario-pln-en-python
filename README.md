# Seminario Procesamiento del Lenguaje Natural con Python

En este repositorio se encuentran los archivos de ejemplo usados en el seminario "Procesamiento del Lenguaje Natural con Python", realizado en la Universitat Rovira i Virgili el 25/02/2022.

## Archivos

En el seminario se usaron 5 Jupyter Notebooks diferentes con ejemplos.
Adicionalmente, en las carpetas _datos_ y _outputs_ se encuentran algunos archivos de datos usados en los ejemplos.

Algunos archivos de datos usados en los Jupyter Notebooks no se encuentran en el repositorio. En los Notebooks se encuentran los enlaces e instrucciones para descargar estos archivos de datos para ser usados en los ejemplos.

## Entorno Python

En el primer Jupyter Notebook se explican dos modos de crear un entorno Python para ejecutar los ejemplos, que son los siguientes:

1. [Conda](https://www.anaconda.com/products/individual) / [Miniconda](https://docs.conda.io/en/latest/miniconda.html) + [Jupyter Lab](https://anaconda.org/conda-forge/jupyterlab)
2. [Google Colab](https://colab.research.google.com/)

## Contenido de los Jupyter Notebooks

Los temas que se tratan en cada uno de los Jupyter Notebooks, son los siguientes:

1. **Variables en Python**: se muestran ejemplos básicos de Python para el manejo y la carga de datos en diferentes formatos. 
2. **Lectura de datos**: se leen datos textuales de diferentes fuentes.
3. **Creación de corpus**: se crean corpus obteniendo los datos de diferentes fuentes de internet.
4. **Pre-procesamiento de texto**: se explican técnicas de preprocesamiento de texto que se aplican previamente a su análisis.
5. **Análisis de datos**: se explican diferentes técnicas de análisis de textos, y se analiza el texto pre-procesado en el notebook anterior.
